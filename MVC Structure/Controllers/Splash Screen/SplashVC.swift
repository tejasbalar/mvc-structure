//
//  SplashVC.swift
//  MVC Structure
//
//  Created by Tejas Patel on 10/10/20.
//

import UIKit

class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
        setUpLayout()
    }

    func initialize(){
        
    }
    
    func setUpLayout(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            let pushController = self.storyboard?.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
            self.navigationController?.pushViewController(pushController, animated: true)
        }
    }
}
